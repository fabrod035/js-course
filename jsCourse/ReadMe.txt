My javascript course June 2020!!!

1. <truthy and false values> is same as <booleans>
2. <logical and &&>, <logical or ||>, <not operator !> are combined with <javascript-learning-logical operators>
3. <sorting> is included in <creating array>
4. <push & pop> <shift & unshift> are combined <push & pop & shift & unshift>
5. <object properties> includes <access, add, update, delete properties of object>
6. <loops> are made by 5 videos
7. functions with multiple args are removed.
8. lexical is explained in block
9. <higher order> will be divided with <map>, <filter>, <reduce>
10. <await> and <async> are combined
11. <axios> and <fetch> are separated
